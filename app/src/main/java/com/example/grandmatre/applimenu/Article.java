package com.example.grandmatre.applimenu;

public class Article {
    protected String reference;
    protected String designation;
    protected String prixUnitHT;
    protected String quantite;

    public Article(){}
    public Article(String uneReference, String uneDesignation, String unPrixUnitHT, String uneQuantite) {
        this.reference=uneReference;
        this.designation=uneDesignation;
        this.prixUnitHT=unPrixUnitHT;
        this.quantite=uneQuantite;
    }


    public String getReference(){ return this.reference; }
        public void setReference(String reference){

        }

        public String getDesignation(){ return this.designation; }
        public void setDesignation(String designation){

        }

        public String getPrixUnitHT(){
            return this.prixUnitHT;
        }
        public void setPrixUnitHT(String prixUnitHT){

        }

        public String getQuantite(){
            return this.quantite;
        }
        public void setQuantite(String quantite){

        }
    }

