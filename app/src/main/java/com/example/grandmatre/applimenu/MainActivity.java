package com.example.grandmatre.applimenu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button ajoutArticle;
    private Button afficheArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialisation();

        testBd();
    }

    public void initialisation(){
        ajoutArticle= (Button)findViewById(R.id.btnAjoutArticle);
        afficheArticle= (Button)findViewById(R.id.btnListeArticle);

    }

    public void testBd(){
       BdAdapter ArticleBdd=new BdAdapter(this);
        Article unArticle = new Article("ref1","des1","10.0","3");
        ArticleBdd.open();
        ArticleBdd.insererArticle(unArticle);
        System.out.println("insertion article");



        Article unArticleFromBdd = ArticleBdd.getArticleWithDesignation("des1");

        if (unArticleFromBdd!=null){
            Toast.makeText(this,unArticleFromBdd.getDesignation(),Toast.LENGTH_LONG).show();
            unArticleFromBdd.setDesignation("des2");
            ArticleBdd.updateArticle(unArticleFromBdd.getReference(),unArticleFromBdd);
        }else{
            Toast.makeText(this,"Article non trouvé",Toast.LENGTH_LONG).show();
        }

        unArticleFromBdd = ArticleBdd.getArticleWithDesignation("des2");

        if (unArticleFromBdd!=null){
            Toast.makeText(this,unArticleFromBdd.getDesignation(),Toast.LENGTH_LONG).show();
            ArticleBdd.removeArticleWithRef(unArticleFromBdd.getReference());
        }

        unArticleFromBdd = ArticleBdd.getArticleWithDesignation("Des2");

        if(unArticleFromBdd == null){
            Toast.makeText(this, "Cet article n'existe pas dans la BDD", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this,"Cet article existe dans la BDD",Toast.LENGTH_LONG).show();
        }
        ArticleBdd.close();
    }



    public void ajout_article(View view){
        startActivity(new Intent(this, ajout_article.class));
    }

    public void liste_article(View view){
        startActivity(new Intent(this, liste_article.class));
    }
}
