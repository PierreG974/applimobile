package com.example.grandmatre.applimenu;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CreatebdArticle extends SQLiteOpenHelper {
    private static final String TABLE_ARTICLE="table_article";
    static final String COL_ID="_id";
    private static String COL_REF="REF";
    private static String COL_DES="DES";
    private static String COL_PU="PU";
    private static String COL_QTE="QTE";

    private static final String CREATE_BDD_= "CREATE TABLE"+TABLE_ARTICLE+"("+COL_ID+"INTEGER PRIMARY KEY AUTOINCREMENT,"+COL_REF+"TEXT NOT NULL,"+COL_DES+"TEXT NOT NULL,"+COL_PU+"TEXT NOT NULL,"+COL_QTE+"TEXT NOT NULL);";

    public CreatebdArticle(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context,name,factory,version);

        //TODO Auto-generated constructor stub

    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL(CREATE_BDD_);
    }

    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
        db.execSQL("DROP TABLE"+TABLE_ARTICLE+";");
    }
}