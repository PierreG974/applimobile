package com.example.grandmatre.applimenu;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;

public class BdAdapter {
    static final int VERSION_BDD=1;
    private static final String NOM_BDD="article.db";
    static final String TABLE_ARTICLE="table_article";
    static final String COL_ID="_id";
    static final int NUM_COL_ID=0;
    static final String COL_REF="REF";
    static final int NUM_COL_REF=1;
    static final String COL_DES="DES";
    static final int NUM_COL_DES=2;
    static final String COL_PU="PU";
    static final int NUM_COL_PU=3;
    static final String COL_QTE="QTE";
    static final int NUM_COL_QTE=4;

    private CreatebdArticle bdArticle;
    private Context context;
    private SQLiteDatabase db;

    public BdAdapter (Context context){
        this.context=context;
        bdArticle=new CreatebdArticle(context,NOM_BDD,null,VERSION_BDD);
    }

    public BdAdapter open(){
        db=bdArticle.getWritableDatabase();
        return this;
    }
    public BdAdapter close(){
        db.close();
        return null;
    }

    public long insererArticle(Article unArticle){
        ContentValues values = new ContentValues();
        values.put(COL_REF,unArticle.getReference());
        values.put(COL_DES,unArticle.getDesignation());
        values.put(COL_PU,unArticle.getPrixUnitHT());
        values.put(COL_QTE,unArticle.getQuantite());

        return db.insert(TABLE_ARTICLE,null,values);
    }

    private Article cursorToArticle(Cursor c){
        if (c.getCount()==0){
            return null;
        }
        else{
            c.moveToFirst();
            Article unArticle=new Article();

            unArticle.setReference(c.getString(NUM_COL_REF));
            unArticle.setDesignation(c.getString(NUM_COL_DES));
            unArticle.setPrixUnitHT(c.getString(NUM_COL_PU));
            unArticle.setQuantite(c.getString(NUM_COL_QTE));
            c.close();
            return unArticle;
        }
    }

    public Article getArticleWithDesignation(String designation){
        Cursor c = db.query(TABLE_ARTICLE,new String[]{COL_ID,COL_REF,COL_DES,COL_PU,COL_QTE},COL_DES+"LIKE\""+designation+"\"",null,null,null,null);
        return cursorToArticle(c);
    }

    public int removeArticleWithRef(String ref){
//Suppression d'un article de la BDD grâce à sa référence
        return db.delete(TABLE_ARTICLE, COL_REF + " = \"" +ref+"\"", null);
    }

    public int updateArticle(String ref, Article unArticle){
        ContentValues values=new ContentValues();
        values.put(COL_DES,unArticle.getDesignation());
        values.put(COL_PU,unArticle.getPrixUnitHT());
        values.put(COL_QTE,unArticle.getQuantite());
        return db.update(TABLE_ARTICLE,values,COL_REF+"=\""+ref+"\"",null);
    }

    public Cursor getData(){
        return db.rawQuery("SELECT * FROM TABLE_ARTICLE",null);
    }
}
