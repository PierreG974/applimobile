package com.example.grandmatre.applimenu;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class liste_article extends AppCompatActivity {

    private ListView listViewArticles;
    private BdAdapter articleBdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
// TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_article);
        listViewArticles = (ListView) findViewById(R.id.ListeViewArticles);
        articleBdd = new BdAdapter(this);
//On ouvre la base de données pour écrire dedans
        articleBdd.open();
        Cursor c = articleBdd.getData();
        Toast.makeText(getApplicationContext(), "il y a "+String.valueOf(c.getCount())+" articles dans la BD",
                Toast.LENGTH_LONG).show();
// colonnes à afficher
        String[] columns = new String[] {BdAdapter.COL_REF, BdAdapter.COL_DES, BdAdapter.COL_PU,
                BdAdapter.COL_QTE};
// champs dans lesquelles afficher les colonnes
        int[] to = new int[] {R.id.ListeViewArticles, R.id.des, R.id.pu, R.id.qte };
        SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(this, R.layout.liste_article,c,columns,to);
// Assign adapter to ListView
        listViewArticles.setAdapter(dataAdapter);
        articleBdd.close();
        Button buttonQuitter = (Button)findViewById(R.id.buttonQuitterListe);
        buttonQuitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
                finish();
            }
        });
    }
}
}
